module.exports = {
    ci: {
        collect: {
            numberOfRuns: 1,
            url: [
                "https://mirumee-new.webflow.io/",
            ],
            startServerReadyPattern: "Application server mounted on",
            settings: {
                chromeFlags: "--disable-dev-shm-usage",
            },
        },
        upload: {
            target: 'lhci',
            serverBaseUrl: 'https://bref-chocolatine-35176.herokuapp.com',
            token: '9ed66d03-761b-4956-8586-3c769f68b04b', // could also use LHCI_TOKEN variable instead
        },
    },
};